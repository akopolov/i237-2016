#include <avr/pgmspace.h>
#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_
#define STUD_NAME "Aleksei Kopõlov"
#define MONTH "Enter Month name first letter >"
#define VER_LIBC "avr-libc version: "__AVR_LIBC_VERSION_STRING__
#define VER_GCC "avr-gcc version:"__VERSION__
#define VER_FW "Version: " GIT_DESCR "-built on: " __DATE__" "__TIME__
#define LICENSE "RFID door lock access control system.\n\r\
Copyright (C) 2016 Aleksei Kopõlov.\n\r\
This program comes with ABSOLUTELY NO WARRANTY.\n\r"
#define HELLO "Use backspace to delete entry and enter to confirm.\n\r\
Arrow key's and del do not work currently.\n\r"
extern PGM_P const name_month[];
#endif /* _HMI_MSG_H */
