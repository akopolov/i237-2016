/*This is a simple program that simulates a terminal and a RFID door lock
access control system.

Copyright (C) 2016  Aleksei Kopõlov

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include "../lib/andygock_avr-uart/uart.h"
#include "hmi_msg.h"
#include "print_helper.h"
#include "../lib/hd44780_111/hd44780.h"
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "../lib/helius_microrl/microrl.h"
#include "cli_microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include <stdlib.h>

#define BAUD 9600
#define F_CPU 16000000UL
#define COUNT_SECONDS // seconds

volatile uint16_t counter_1;
//store ID of the last used card.
uint8_t old_card[10];

// Create microrl object and pointer on it
microrl_t rl;
microrl_t *prl = &rl;

static inline void init_uart(void)
{
    /*ste pin 25 and 23*/
    DDRA |= _BV(DDA3);
    DDRA |= _BV(DDA1);
    //uart init
    uart0_init(UART_BAUD_SELECT(BAUD, F_CPU));
    uart3_init(UART_BAUD_SELECT(BAUD, F_CPU));
}

static inline void init_lcd(void)
{
    //lcd init
    lcd_init();
    lcd_clrscr();
}

static inline void init_hello(void)
{
    uart0_puts_p(PSTR(LICENSE));
    uart0_puts_p(PSTR(HELLO));
}

static inline void init_rl(void)
{
    //RL init
    microrl_init (prl, uart0_puts);
    microrl_set_execute_callback (prl, cli_execute);
}

static inline void init_rfid_reader(void)
{
    /*Init RFID-RC522*/
    MFRC522_init();
    PCD_Init();
}

static inline void init_counter(void)
{
    counter_1 = 0; //Reset counter
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); //Turn on CTC (Clear Timer on Compare)
#ifdef COUNT_SECONDS
    TCCR1B |= _BV(CS12); //fCPU/256
    OCR1A = 62549; //Note that it is actually two registers OCR5AH and OCR5AL
#endif /*COUNT_SECONDS*/
    TIMSK1 |= _BV(OCIE1A);
}

static inline void user(void)
{
    uart3_puts_p(PSTR(STUD_NAME"\n\r"));
    uart3_puts_p(PSTR(VER_FW "\n\r"));
    uart3_puts_p(PSTR(VER_LIBC " " VER_GCC "\n\r"));
}

static inline void lcd_print(void)
{
    lcd_puts_P(PSTR(STUD_NAME));
    lcd_goto(0x40);
}

static inline void led_on_off(void)
{
    /* set pin 3 high to turn led on/off */
    PORTA ^= _BV(PORTA3);
}

static inline void heartbeat (void)
{
    static uint16_t counter_1_prev;
    uint16_t counter_1_cpy = 0;
    char buffer[15];
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        counter_1_cpy = counter_1;
    }

    if (counter_1_cpy != counter_1_prev) {
        sprintf(buffer, "Timer:%u s\n\r", counter_1_cpy);
        uart3_puts(buffer);
        counter_1_prev = counter_1_cpy;
        led_on_off();
    }
}
static inline void read_rfid(void)
{
    static uint16_t start;
    uint16_t counter_1_cpy = 0;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        counter_1_cpy = counter_1;
    }

    //read RFID
    if (PICC_IsNewCardPresent()) {
        start = counter_1_cpy;
        Uid uid;
        Uid *uid_ptr = &uid;
        PICC_ReadCardSerial(uid_ptr);

        if (memcmp(uid.uidByte, old_card, uid.size) != 0) {
            lcd_clr(0x40, 16);
            lcd_goto(0x40);
            const char *user = get_username(uid.uidByte);

            if (user != NULL) {
                lcd_puts(user);
                //turn led in pin 23 ON
                PORTA |= _BV(PORTA1);
            } else {
                lcd_puts("Access denied");
                PORTA &= ~_BV(PORTA1);
            }

            for (byte i = 0; i < uid.size; i++) {
                old_card[i] = uid.uidByte[i];
            }
        }
    }

    //clear old_card arrey every 2 sec (prevents LCD from constantly printing 1 and the same line)
    if (counter_1_cpy - start == 2) {
        old_card[0] = '\0';
    }

    //turn led in pin 23 OFF after 3 sec
    if (counter_1_cpy - start == 3) {
        PORTA &= ~_BV(PORTA1);
    }

    //clear screen after 5 sec
    if (counter_1_cpy - start == 5) {
        lcd_clr(0x40, 16);
    }
}

int main (void)
{
    init_uart();
    init_lcd();
    //sei init
    sei();
    init_hello();
    init_rl();
    init_rfid_reader();
    init_counter();
    user();
    lcd_print();

    while (1) {
        //CLI commands are handled in cli_execute()
        microrl_insert_char(prl, uart0_getc() & 0x00FF);
        read_rfid();
        heartbeat();
    }
}

ISR(TIMER1_COMPA_vect)
{
    counter_1++;
}

