#include <avr/pgmspace.h>
#include "hmi_msg.h"
const char s1[] PROGMEM = "Jaanuar";
const char s2[] PROGMEM = "Veebruar";
const char s3[] PROGMEM = "Märts";
const char s4[] PROGMEM = "Aprill";
const char s5[] PROGMEM = "Mai";
const char s6[] PROGMEM = "Juuni";
PGM_P const name_month[] PROGMEM = {s1, s2, s3, s4, s5, s6};
